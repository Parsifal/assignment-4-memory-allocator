#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <sys/mman.h>

#define HEAP_SIZE 8175
#define BLOCK_SIZE 50

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*)((uint8_t*)contents - offsetof(struct block_header, contents));
}

static void* initialize_test(const char* test_name, size_t heap_size) {
    printf("=== %s ===\n", test_name);
    void* heap = heap_init(heap_size);
    debug_heap(stdout, heap);
    return heap;
}

static void* allocate_block(void* heap, size_t block_size) {
    printf("Allocating block of size %zu:\n", block_size);
    void* block = _malloc(block_size);
    debug_heap(stdout, heap);
    return block;
}

static void free_block(void* heap, void* block) {
    printf("Freeing block:\n");
    _free(block);
    debug_heap(stdout, heap);
}

void test1() {
    void* heap = initialize_test("Test Successful Allocation and Free", HEAP_SIZE);

    void* block = allocate_block(heap, BLOCK_SIZE);
    assert(block);

    free_block(heap, block);
    assert(block_get_header(block)->is_free);

    heap_term();
    printf("Test passed.\n\n");
}

void test2() {
    void* heap = initialize_test("Freeing one block from several allocated ones", HEAP_SIZE);

    void* block1 = allocate_block(heap, BLOCK_SIZE);
    void* block2 = allocate_block(heap, BLOCK_SIZE);
    void* block3 = allocate_block(heap, BLOCK_SIZE);

    free_block(heap, block2);

    assert(block_get_header(block2)->is_free);
    assert(!block_get_header(block1)->is_free);
    assert(!block_get_header(block3)->is_free);

    free_block(heap, block1);
    free_block(heap, block3);
    assert(block_get_header(block1)->is_free);
    assert(block_get_header(block3)->is_free);

    heap_term();
    printf("Test passed.\n\n");
}

void test3() {
    void* heap = initialize_test("Freeing two blocks from several allocated ones", HEAP_SIZE);

    void* block1 = allocate_block(heap, BLOCK_SIZE);
    void* block2 = allocate_block(heap, BLOCK_SIZE);
    void* block3 = allocate_block(heap, BLOCK_SIZE);

    free_block(heap, block2);
    free_block(heap, block1);

    assert(block_get_header(block2)->is_free);
    assert(block_get_header(block1)->is_free);
    assert(block_get_header(block1)->capacity.bytes == BLOCK_SIZE + size_from_capacity((block_capacity){ BLOCK_SIZE }).bytes);

    free_block(heap, block3);
    assert(block_get_header(block3)->is_free);

    heap_term();
    printf("Test passed\n\n");
}

void test4() {
    void* heap = initialize_test("The memory has run out, the new memory region expands the old one", HEAP_SIZE);

    void* block1 = allocate_block(heap, HEAP_SIZE);
    assert(!block_get_header(block1)->is_free);
    void* block2 = allocate_block(heap, BLOCK_SIZE);
    assert(!block_get_header(block2)->is_free);

    free_block(heap, block1);
    assert(block_get_header(block1)->is_free);
    free_block(heap, block2);
    assert(block_get_header(block2)->is_free);
    heap_term();
    printf("Test passed\n\n");
}

void test5() {
    void* heap = initialize_test("The memory has run out, the old memory region cannot be expanded due to a different allocated address range, the new region is allocated elsewhere", HEAP_SIZE);

    void* addr = mmap( heap, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS , -1, 0);
    assert(addr != MAP_FAILED);
    debug_heap(stdout, heap);

    void* block = allocate_block(heap, BLOCK_SIZE);
    assert(block);

    free_block(heap, block);
    assert(block_get_header(block)->is_free);

    heap_term();
    printf("Test passed\n\n");
}



int main() {
    test1();
    test2();
    test3();
    test4();
    test5();
    printf("All tests passed.\n");
}